#!/usr/bin/env python

###########################################
############# A Simple publisher node #####
# Creates a test node for publishing a string
# Author : Mythra
###########################################
import rospy
from std_msgs.msg import String

class publi():
    def __init__(self):
        self.wstr = "Yo"
        self.pblr = rospy.Publisher('str_pub', String, queue_size=10)
        rospy.init_node('Publisher')
    def publish_str(self):
        r = rospy.Rate(20)
        while not rospy.is_shutdown():
            self.pblr.publish(self.wstr)            
            r.sleep()
if __name__ == "__main__":
    pub = publi()
    pub.publish_str()

